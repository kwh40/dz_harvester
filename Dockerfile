FROM python:3.7

RUN mkdir app

WORKDIR /app

ADD configs/komatsu_harvester.json configs/komatsu_harvester.json
ADD requirements.txt requirements.txt
ADD main.py main.py
RUN pip install -r requirements.txt

CMD ["python", "main.py"]
