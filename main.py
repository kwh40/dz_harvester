import ml
from ml.fml40.features.functionalities.provides_production_data import ProvidesProductionData
from config import *
import os
import time
import sys
import geocoder
import gps 

ml.setup_logger("komatsu_harvester")

if getattr(sys, 'frozen', False):
    path = os.path.dirname(sys.executable)
elif __file__:
    path = os.path.dirname(__file__)
config_path = os.path.abspath(os.path.join(path, "configs"))

dt_model = ml.load_config(config_filepath=os.path.join(config_path, "komatsu_harvester.json"))

thing = ml.create_thing(model=dt_model, grant_type="client_credentials",
                        secret=secret, is_broker_rest=False,
                        is_broker=True, is_repo=False, is_stanford2010=True,
                        is_stanford2010_sync=is_stanford2010_sync,
                        stanford2010_sync_freq=query_frequency,
                        stanford2010_path=stanford2010_path)


def simulate_location():
    my_location = thing.features["ml40::Location"]
    while True:
        gpsd = gps.gps()
        gpsd.stream(gps.WATCH_ENABLE|gps.WATCH_NEWSTYLE)
        for report in gpsd:
            if report['class'] == "TPV":
                my_location.latitude = report['lat']
                my_location.longitude = report['lon']
                break 
        """
        g = geocoder.ip('me')
        if g.latlng is not None:
            my_location.longitude = g.latlng[0]
            my_location.latitude = g.latlng[1]
        """
        time.sleep(10)


class ProvidesProductionDataImpl(ProvidesProductionData):
    def __init__(self, name="", identifier=""):
        super().__init__(name=name,
                         identifier=identifier)

    def getNumberOfTrees(self, treeSpeciesName):
        treeSpeciesProductKey = 0
        species_def_nodes = thing.stanford2010.find_nodes(path="Machine/SpeciesGroupDefinition")
        for node in species_def_nodes:
            for i in node.iter():
                if "SpeciesGroupName" in i.tag:
                    if i.text != treeSpeciesName:
                        continue
                    for j in node.iter():
                        if "SpeciesGroupKey" in j.tag:
                            treeSpeciesProductKey = j.text
        xpath = "Machine/Stem/SpeciesGroupKey[.='{}']".format(treeSpeciesProductKey)
        node_list = thing.stanford2010.find_nodes(path=xpath)
        return len(node_list)

    def getNumberOfSegments(self, assortmentProductName):
        assortmentProductKey = 0
        product_def_nodes = thing.stanford2010.find_nodes(path="Machine/ProductDefinition")
        for node in product_def_nodes:
            for i in node.iter():
                if "ProductName" in i.tag:
                    if i.text != assortmentProductName:
                        continue
                    for j in node.iter():
                        if "ProductKey" in j.tag:
                            assortmentProductKey = j.text
        xpath = "Machine/Stem/SingleTreeProcessedStem/Log/ProductKey[.='{}']".format(assortmentProductKey)
        node_list = thing.stanford2010.find_nodes(path=xpath)
        return len(node_list)

    def getProductionData(self, xpath):
        node_list = thing.stanford2010.find_nodes(path=xpath)
        for node in node_list:
            counter = 0
            for i in node.iter():
                counter += 1
            if counter == 1:
                return node.text
            else:
                return thing.stanford2010.to_string(root=node)

        return "Not Found"


thing.add_user_def(func=simulate_location)
ml.add_function_impl_obj(thing, ProvidesProductionDataImpl, "fml40::ProvidesProductionData")
thing.run_forever()
